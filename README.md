# PIXI.js Fractal renderer

A simple and documented fractal renderer using Canvas API

Intended to demonstrate the math and techniques behind rendering fractals using computers

By default renders the Mandelbrot set

# Usage

Open index.html in any browser

Click any location to center the viewport

Press 'z' to zoom and 'a' to zoom out.  Precision is limited as you would expect, but an impressive level of zoom can be achieved

Press '+' or '-' to increase or decrease cycles.  Default cycle count is good enough for the default zoom level, but more cycles are required when zooming.

getColor can be altered to experiment with coloring schemes
